﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class NavBarModel
    {
        public string SelectedRange { get; set; }
        public string HighlightedText { get; set; }
        public string LocalCounterRange { get; set; }
        public string GlobalCounterRange { get; set; }
    }
}