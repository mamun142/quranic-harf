﻿using Common;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;


namespace Web.Models
{
    public class SearchFilter
    {
        public int StartSurahNo { get; set; }       // q:ss
        public int EndSurahNo { get; set; }         // q:es
        public int StartVerseNo { get; set; }       // q:sv
        public int EndVerseNo { get; set; }         // q:ev
        public string SpecificHarf { get; set; }    // q:h
        public string SpecificWord { get; set; }    // q:w
        public int SourceId { get; set; }        // 

        public SearchFilter()
        {
            StartSurahNo = 1;
            StartVerseNo = 1;
            SourceId = 1;
            CorrectMissingValues();
        }

        public SearchFilter Parse(NameValueCollection queryString)
        {
            if (queryString.AllKeys.Contains("ss"))
            {
                StartSurahNo = int.Parse(queryString["ss"]);
            }
            if (queryString.AllKeys.Contains("es"))
            {
                EndSurahNo = int.Parse(queryString["es"]);
            }
            if (queryString.AllKeys.Contains("sv"))
            {
                StartVerseNo = int.Parse(queryString["sv"]);
            }
            if (queryString.AllKeys.Contains("ev"))
            {
                EndVerseNo = int.Parse(queryString["ev"]);
            }
            if (queryString.AllKeys.Contains("h"))
            {
                SpecificHarf = queryString["h"];
            }
            if (queryString.AllKeys.Contains("w"))
            {
                SpecificWord = queryString["w"];
            }
            if (queryString.AllKeys.Contains("src"))
            {
                SourceId = int.Parse(queryString["src"]);
            }


            CorrectMissingValues();
            return this;
        }

        private void CorrectMissingValues()
        {
            if (StartSurahNo <= 0 || StartSurahNo > 114) StartSurahNo = 1;
            if (StartVerseNo <= 0) StartVerseNo = 1;
            if (StartVerseNo > Constants.SurahVerseCounter[StartSurahNo]) StartVerseNo = Constants.SurahVerseCounter[StartSurahNo];

            if (EndSurahNo <= 0 || EndSurahNo > 114 || EndSurahNo < StartSurahNo) EndSurahNo = StartSurahNo;
            if (EndVerseNo <= 0) EndVerseNo = Constants.SurahVerseCounter[EndSurahNo];
            if (EndVerseNo > Constants.SurahVerseCounter[EndSurahNo]) EndVerseNo = Constants.SurahVerseCounter[EndSurahNo];
        }

        public string GenerateUrl(string baseUrl)
        {
            NameValueCollection args = HttpUtility.ParseQueryString(baseUrl);
            if (StartSurahNo > 0) args["ss"] = StartSurahNo.ToString();
            if (StartVerseNo > 0) args["sv"] = StartVerseNo.ToString();
            if (EndSurahNo > 0) args["es"] = EndSurahNo.ToString();
            if (EndVerseNo > 0) args["ev"] = EndVerseNo.ToString();
            if (string.IsNullOrWhiteSpace(SpecificHarf)) args["h"] = SpecificHarf;
            if (string.IsNullOrWhiteSpace(SpecificWord)) args["w"] = SpecificWord;

            if (args.AllKeys.Length > 0)
                return baseUrl + "?" + args.ToString();

            return baseUrl;
        }

        public void AutoLoad()
        {
            // HttpContext.Current.Request 

            CorrectMissingValues();
        }
    }
}