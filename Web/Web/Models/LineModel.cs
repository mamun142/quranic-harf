﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace Web.Models
{
    public class LineModel
    {
        public int VerseId { get; set; }
        public int VerseNo { get; set; }
        public int SurahNo { get; set; }
        public VerseHarf[] Harfs { get; set; }
    }

}