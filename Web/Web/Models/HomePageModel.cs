﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Data;

namespace Web.Models
{
    public class HomePageModel : ISurahSearchable
    {
        public HarfInBoxModel HarfInBoxModel { get; set; }
        public NavBarModel NavBarModel { get; set; }
        public string WarningMessage { get; set; }
        public string ErrorMessage { get; set; }

        public SearchFilter SearchFilter { get; set; }
        public string SearchToUrl { get; set; }

        public List<Surah> SurahList { get; set; }

        public List<string> HarfFilterItems { get; set; }

        public List<KeyValuePair<int, string>> SourceItems { get; set; }
    }
}