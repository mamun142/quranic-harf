﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class HarfInBoxModel
    {
        public List<LineModel> AllLines { get; set; }
        public bool ShowEditIcon { get; set; }
    }
}

