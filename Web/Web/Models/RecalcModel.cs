﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class RecalcModel
    {
        public bool IsProcessStarted { get; set; }
        public bool IsProcessRunning { get; set; }
        public bool IsRecalcRequired { get; set; }
        public string RecalcDoneUpto { get; set; }
    }
}

