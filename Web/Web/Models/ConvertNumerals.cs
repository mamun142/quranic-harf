﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class ConvertNumerals
    {
        public static string Convert(string input)
        {
            string[] Arabic = new string[10] { "۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹" };
            for (int j = 0; j < Arabic.Length; j++)
            {
                input = input.Replace(j.ToString(), Arabic[j]);
            }

           return (input);
        }
    }
}
