﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class EditModel
    {
        public int VerseId { get; set; }
        public string VerseText { get; set; }
        public string OriginalText { get; set; }
        public string OriginalVerseText { get; set; }

        public string ReturnUrl { get; set; }

        public string SuccessMessage { get; set; }
        public string WarningMessage { get; set; }

        public int SurahNo { get; set; }
        public string SurahName { get; set; }
        public int VerseNo { get; set; }

        public bool? ConfirmCheck { get; set; }
        public EditingState CurrentState { get; set; }
        public EditingState RequestedState { get; set; }

        public HarfInBoxModel OriginalLines { get; set; }
        public HarfInBoxModel PreviewLines { get; set; }

        public VerseEditHistoryModel HistoryModel { get; set; }
    }
}