﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class VerseEditHistoryModel
    {
        public List<VerseEditHistory> Histories { get; set; }
    }
}
