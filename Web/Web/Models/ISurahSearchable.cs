﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public interface ISurahSearchable
    {
        SearchFilter SearchFilter { get; set; }
        string SearchToUrl { get; set; }
        List<Surah> SurahList { get; set; }
        List<string> HarfFilterItems { get; set; }
        List<KeyValuePair<int, string>> SourceItems { get; set; }
    }
}
