﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Data;

namespace Web.Models
{
    public class StatModel : ISurahSearchable
    {
        public string HarfLabels { get; set; }
        public string HarfData { get; set; }
        public List<HarfPositions> AllHarfPositions { get; set; }

        public SearchFilter SearchFilter { get; set; }

        public string SearchToUrl { get; set; }

        public List<Surah> SurahList { get; set; }

        public List<string> HarfFilterItems { get; set; }

        public List<KeyValuePair<int, string>> SourceItems { get; set; }
    }

    public class HarfPositions
    {
        public string Harf { get; set; }
        public List<int> LocalPositions { get; set; }
        public List<int> GlobalPositions { get; set; }
    }
}