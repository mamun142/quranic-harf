﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Data;
using System.Configuration;
using Web.Models;
using Common;

namespace Web.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            SearchFilter searchFilter = CalculateSearchFilter();

            List<Verse> verses = GetSearchQueryData(searchFilter);

            List<int> localPositions = GenerateLetterPosition(verses, searchFilter.SpecificHarf, false);
            ViewBag.LocalHarfPositions = localPositions;
            List<int> globalPositions = GenerateLetterPosition(verses, searchFilter.SpecificHarf, true);
            ViewBag.GlobalHarfPositions = globalPositions;

            List<LineModel> allLines = new List<LineModel>();

            foreach (Verse verse in verses)
            {
                var line = GenerateLine(verse);
                allLines.Add(line);
            }
            HomePageModel model = new HomePageModel()
            {
                HarfInBoxModel = new HarfInBoxModel()
                {
                    AllLines = allLines,
                    ShowEditIcon = true
                },
                NavBarModel = new NavBarModel()
                {
                    HighlightedText = $"{searchFilter.SpecificHarf}",
                    SelectedRange = $"{searchFilter.StartSurahNo}:{searchFilter.StartVerseNo}-{searchFilter.EndSurahNo}:{searchFilter.EndVerseNo}"
                },
                SearchFilter = searchFilter,
                SearchToUrl = Url.Action("Index", "Home"),
                SourceItems = GetAllSourceItems(),
                SurahList = GetAllSurah(),
                HarfFilterItems = GetAllUniqueHarf()
            };

            if (allLines != null && allLines.Count > 0)
            {
                VerseHarf firstHarf = allLines.First().Harfs.First();
                VerseHarf lastHarf = allLines.Last().Harfs.Last();

                if(firstHarf != null && lastHarf != null)
                {
                    model.NavBarModel.GlobalCounterRange = $"{firstHarf.GlobalCounter}-{lastHarf.GlobalCounter}";
                    model.NavBarModel.LocalCounterRange = $"{firstHarf.LocalCounter}-{lastHarf.LocalCounter}";
                }
            }

            TryAddWarningMessage(model);
            return View(model);
        }

        private void TryAddWarningMessage(HomePageModel model)
        {
            using (var context = new QuranicHarfDbContext())
            {
                var verses = context.Verses.Where(v => v.SourceId == _sourceId && v.IsRecalculationRequired).OrderBy(v => v.SurahNo).ThenBy(v => v.VerseNo).ToList();
                if (verses != null && verses.Count > 0)
                {
                    model.WarningMessage = $"Counter shown here might be wrong. Recalculation required starting from surah {verses[0].SurahNo} verse {verses[0].VerseNo}, because there was an edit at {verses[0].EditedDate.ToString()}." + Environment.NewLine;
                }

                // DateTime? lastEditDate = context.Verses.Max(v => v.EditedDate);
                var lastUpdateConfig = context.Configs.SingleOrDefault(c => c.Key == ConfigKey.LastFixCounterRunDate.ToString());
                if (string.IsNullOrWhiteSpace(lastUpdateConfig.Value))
                {
                    model.WarningMessage = "Harf counter is never calculated. Please run the calculator before your analysis.";
                }

                bool lastRunSuccess;
                var isLastRunSuccessConfig = context.Configs.SingleOrDefault(c => c.Key == ConfigKey.IsLastFixCounterRunSuccess.ToString());
                if (bool.TryParse(isLastRunSuccessConfig.Value, out lastRunSuccess) && !lastRunSuccess)
                {
                    model.ErrorMessage = $"Your last counter calculator failed at {lastUpdateConfig.Value}";
                }
            }
        }

        public ActionResult Preview()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}