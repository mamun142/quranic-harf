﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Data;
using Web.Models;

namespace Web.Controllers
{    
    public class RecalcController : BaseController
    {
        // this function should be asynchronous, it should respond immediately to the client
        // leaving the server do the process behind the scean 
        // 
        public JsonResult Try()
        {
            // Create a database table which will store progress of process per source 
            // table [RecalcProgress] will contain - SourceId, ProcessStartDate, StartSurahNo, StartVerseNo, IsInProgress

            SearchFilter searchFilter = CalculateSearchFilter();

            using (var context = new QuranicHarfDbContext())
            {
                // check whether a process already running 

                // If no process running initiate a new process 

                // if a process is already running, just return the current state, without creating another process 

                // If current source has noting to calculate, then try other sources

                RecalcModel model = new RecalcModel()
                {
                    IsProcessRunning = true,
                    IsProcessStarted = false,
                    IsRecalcRequired = true,
                    RecalcDoneUpto = "2:192"
                };
                return Json(model, JsonRequestBehavior.AllowGet);
            }
        }
    }
}