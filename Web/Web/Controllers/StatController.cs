﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common;
using Web.Models;

namespace Web.Controllers
{
    public class StatController : BaseController
    {
        public ActionResult Index()
        {
            SearchFilter searchFilter = CalculateSearchFilter();
            var allVerses = GetSearchQueryData(searchFilter);

            int count = Constants.HarfGroups.Count();
            var harfs = Constants.HarfGroups.Select(h => h[0]);
            List<int> harfCounts = new List<int>(count);
            List<HarfPositions> allPositions = new List<HarfPositions>();
            foreach (string harf in harfs)
            {
                HarfPositions harfPosition = new HarfPositions()
                {
                    Harf = harf,
                    LocalPositions = GenerateLetterPosition(allVerses, harf, false),
                    GlobalPositions = GenerateLetterPosition(allVerses, harf, true)
                };
                harfCounts.Add(harfPosition.LocalPositions.Count());
                allPositions.Add(harfPosition);
            }

            StatModel model = new StatModel()
            {
                HarfLabels = String.Join(", ", harfs.Select(h => "\"" + h + "\"")),
                HarfData = string.Join(", ", harfCounts),
                AllHarfPositions = allPositions,
                SearchFilter = searchFilter,
                SearchToUrl = Url.Action("Index", "Stat"),
                SourceItems = GetAllSourceItems(),
                SurahList = GetAllSurah(),
                HarfFilterItems = GetAllUniqueHarf()
            };

            return View(model);
        }
    }
}