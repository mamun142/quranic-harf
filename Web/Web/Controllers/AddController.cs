﻿using Common;
using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;

namespace Web.Controllers
{
    [Authorize]
    public class AddController : BaseController
    {
        [HttpGet]
        public ActionResult Index(int verseId, string returnUrl)
        {
            SearchFilter searchFilter = CalculateSearchFilter();

            using (var context = new QuranicHarfDbContext())
            {
                var verse = context.Verses.Find(verseId);
                var model = new EditModel()
                {
                    VerseId = verse.Id,
                    OriginalText = verse.OriginalText,
                    VerseText = verse.VerseText,
                    OriginalVerseText = verse.VerseText,
                    ReturnUrl = returnUrl,
                    SurahName = context.Surahs.SingleOrDefault(s => s.SurahNo == verse.SurahNo).EnglishName,
                    SurahNo = verse.SurahNo,
                    VerseNo = verse.VerseNo,
                    CurrentState = EditingState.Edit
                };
                model.HistoryModel = GetHistory(verse ,context);

                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Index(EditModel model)
        {
            using (var context = new QuranicHarfDbContext())
            {
                var verse = context.Verses.Find(model.VerseId);
                if (model.OriginalVerseText == model.VerseText)
                {
                    model.WarningMessage = "Nothing to save. Please make valid changes before clicking \"Preview\" button. Otherwise click \"Cancel\" to return to previous page.";
                }
                else if (model.CurrentState == EditingState.Edit && model.RequestedState == EditingState.Preview)
                {
                    // harfs = harfsCache = TextFormatter.ParseWithHarkat(originalVerseText);
                    model.OriginalLines = new HarfInBoxModel()
                    {
                        AllLines = new List<LineModel>() { GenerateLine(verse) },
                        ShowEditIcon = false
                    };
                    model.PreviewLines = new HarfInBoxModel()
                    {
                        AllLines = new List<LineModel>() { GenerateLineForPreview(verse, model.VerseText) },
                        ShowEditIcon = false
                    };
                    model.CurrentState = EditingState.Preview;
                }
                else if (model.CurrentState == EditingState.Preview && model.RequestedState == EditingState.Save && model.ConfirmCheck == true)
                {
                    verse.VerseText = model.VerseText;
                    verse.EditedDate = DateTime.UtcNow;
                    verse.EditedBy = HttpContext.User.Identity.Name;

                    string[] harfs = TextFormatter.ParseWithHarkat(model.VerseText);
                    int harfCount = harfs.Where(h => h != " ").Count();
                    if (harfCount == verse.HarfCount)
                    {
                        DoVerseCounterRecalculation(verse, context, harfs);
                        verse.IsRecalculationRequired = false;
                    }
                    else
                    {
                        verse.IsRecalculationRequired = true;
                    }

                    SearchFilter searchFilter = CalculateSearchFilter();
                    // TODO: show sourceName in edit page 
                    var editHistory = new VerseEditHistory()
                    {
                        SourceId = searchFilter.SourceId,
                        VerseNo = verse.VerseNo,
                        SurahNo = verse.SurahNo,
                        EditedText = verse.VerseText,
                        EditedBy = HttpContext.User.Identity.Name,
                        EditedDate = DateTime.UtcNow
                    };

                    context.VerseEditHistories.Add(editHistory);
                    context.SaveChanges();

                    model.SuccessMessage = "Your changes saved successfully.";
                    model.OriginalText = verse.OriginalText;
                    model.CurrentState = EditingState.Save;

                }
                else if (model.CurrentState == EditingState.Preview && model.RequestedState == EditingState.Edit)
                {
                    model.CurrentState = EditingState.Edit;
                }
                // Create a partial _VerseEditHistory view which will take EditHistoryModel 
                model.HistoryModel = GetHistory(verse, context);
            }
            return View(model);
        }

        private VerseEditHistoryModel GetHistory(Verse verse,QuranicHarfDbContext context)
        {
            return new VerseEditHistoryModel()
            {
                Histories = context.VerseEditHistories.Where(VE => VE.VerseNo == verse.VerseNo && VE.SurahNo == verse.SurahNo && VE.SourceId == verse.SourceId).OrderByDescending(h => h.EditedDate).ToList()
            };
        }

        private void DoVerseCounterRecalculation(Verse verse, QuranicHarfDbContext context, string[] harfs)
        {
            context.VerseHarfs.RemoveRange(context.VerseHarfs.Where(h => h.VerseId == verse.Id));
            int verseHarfSerial = 1;
            int verseHarfCounter = 0;
            foreach (string harf in harfs)
            {
                var item = new VerseHarf()
                {
                    HarfText = harf,
                    Serial = verseHarfSerial++,
                    VerseId = verse.Id,
                    LocalCounter = harf == " " ? 0 : verse.StartSurahCounter + verseHarfCounter,
                    GlobalCounter = harf == " " ? 0 : verse.StartGlobalCounter + verseHarfCounter
                };
                if (harf != " ") verseHarfCounter++;
                context.VerseHarfs.Add(item);
            }
        }

    }
}