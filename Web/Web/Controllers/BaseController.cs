﻿using Common;
using Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;

namespace Web.Controllers
{
    public class BaseController : Controller
    {
        protected int _sourceId;

        public BaseController()
        {
            _sourceId = int.Parse(ConfigurationManager.AppSettings["DefaultSourceId"]);
        }

        protected SearchFilter CalculateSearchFilter()
        {
            SearchFilter searchFilter = new SearchFilter();
            searchFilter.SourceId = _sourceId;
            searchFilter.Parse(Request.QueryString);
            _sourceId = searchFilter.SourceId;
            return searchFilter;
        }

        protected List<Verse> GetSearchQueryData(SearchFilter searchFilter)
        {
            using (QuranicHarfDbContext context = new QuranicHarfDbContext())
            {
                var query = context.Verses.Where(v => v.SurahNo >= searchFilter.StartSurahNo && v.SurahNo <= searchFilter.EndSurahNo && v.SourceId == searchFilter.SourceId);
                query = query.Except(context.Verses.Where(v => v.SurahNo == searchFilter.StartSurahNo && v.VerseNo < searchFilter.StartVerseNo));
                query = query.Except(context.Verses.Where(v => v.SurahNo == searchFilter.EndSurahNo && v.VerseNo > searchFilter.EndVerseNo));
                return query.ToList();
            }
        }

        protected List<Surah> GetAllSurah()
        {
            List<Surah> surahs = default(List<Surah>);
            using (QuranicHarfDbContext context = new QuranicHarfDbContext())
            {
                surahs = context.Surahs.ToList();
            }
            return surahs;
        }

        protected LineModel GenerateLine(Verse verse)
        {
            using (var dbContext = new QuranicHarfDbContext())
            {
                LineModel line = new LineModel()
                {
                    VerseId = verse.Id,
                    SurahNo = verse.SurahNo,
                    VerseNo = verse.VerseNo,
                    Harfs = dbContext.VerseHarfs.Where(h => h.VerseId == verse.Id).OrderBy(h => h.Serial).ToArray()
                };
                return line;
            }
        }

        protected List<KeyValuePair<int, string>> GetAllSourceItems()
        {
            var items = new List<KeyValuePair<int, string>>();

            using (var context = new QuranicHarfDbContext())
            {
                var sources = context.Sources.ToList();
                var keyValyeItems = sources.Select(s => new KeyValuePair<int, string>(s.Id, s.Name));
                items.AddRange(keyValyeItems);
            }

            return items;
        }

        protected List<string> GetAllUniqueHarf()
        {
            List<string> harfFilterItems = new List<string>();

            harfFilterItems.AddRange(Constants.Harfs.Select(h => h.ToString()));
            harfFilterItems.AddRange(Constants.AdditionalHarfs.Select(h => h.ToString()));
            harfFilterItems.AddRange(Constants.SpecialHarfs.Select(h => h.ToString()));

            using (var context = new QuranicHarfDbContext())
            {
                var query = (
                    from vh in context.VerseHarfs
                    join v in context.Verses on vh.VerseId equals v.Id
                    where v.SourceId == _sourceId
                    select vh.HarfText
                    ).Distinct();

                harfFilterItems.AddRange(query.ToList());
            }

            return harfFilterItems.Distinct().ToList();
        }

        protected LineModel GenerateLineForPreview(Verse verse, string newVerseText)
        {
            int surahCounter = verse.StartSurahCounter;
            int globalCounter = verse.StartGlobalCounter;
            int serialNo = 0;

            LineModel line = new LineModel()
            {
                VerseId = verse.Id,
                SurahNo = verse.SurahNo,
                VerseNo = verse.VerseNo
            };

            List<VerseHarf> verseHarfs = new List<VerseHarf>();
            foreach(string h in TextFormatter.ParseWithHarkat(newVerseText))
            {
                VerseHarf vHarf = new VerseHarf()
                {
                    LocalCounter = string.IsNullOrWhiteSpace(h) ? 0 : surahCounter++,
                    GlobalCounter = string.IsNullOrWhiteSpace(h) ? 0 : globalCounter++,
                    HarfText = h,
                    Serial = ++serialNo,
                    VerseId = verse.Id
                };
                verseHarfs.Add(vHarf);
            }
            line.Harfs = verseHarfs.ToArray();

            return line;
        }

        protected List<int> GenerateLetterPosition(List<Verse> verses, string searchHarf, bool isGlobalCounter)
        {
            List<int> positions = new List<int>();
            if (string.IsNullOrWhiteSpace(searchHarf)) return positions;

            using (var context = new QuranicHarfDbContext())
            {
                foreach (Verse verse in verses)
                {
                    var verseharfs = context.VerseHarfs.Where(vh => vh.VerseId == verse.Id).ToList();
                    foreach (var verseHarf in verseharfs)
                    {
                        // List<string> selectedGroup 
                        bool matchFound = false;
                        foreach (var group in Constants.HarfGroups)
                        {
                            foreach (string groupHarf in group)
                            {
                                if (verseHarf.HarfText.Contains(groupHarf) && group.Contains(searchHarf))
                                {
                                    positions.Add(isGlobalCounter ? verseHarf.GlobalCounter : verseHarf.LocalCounter);
                                    matchFound = true;
                                    break;
                                }
                            }
                            if (matchFound)
                            {
                                break;
                            }
                        }
                    }
                }
            }

            return positions;
        }

    }
}