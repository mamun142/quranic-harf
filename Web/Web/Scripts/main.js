﻿
// Template of item 
//{
//    selector: "",
//    delay: 1000,
//    preCallback: function () { },
//    postCallback: function () { },
//    attr: {
//        "title": "Sample title",
//        "content": "A single harf of the verse",
//        "placement": "bottom", //  [top,bottom,left,right] 
//        "html": "",   // true|false 
//        "step": "1"
//        }
//}

var StartGuidedTour = function (page) {
    var items = [];
    var isHome = page === "Home";
    var verseLine = page === "Home";
    var navFilter = page === "Home";
    var isStat = page === "Stat";
    var isEdit = page === "Edit";

    if (verseLine) {
        items.push({
            selector: ".row.verse-line:first-child",
            attr: {
                "content": "A verse is shown.",
                "placement": "bottom",
            }
        });
        items.push({
            selector: ".verse-line:nth-child(2) div.single-harf:nth-child(3)",
            attr: {
                "content": "A single harf of the verse",
                "placement": "bottom",
            }
        });
        items.push({
            selector: ".verse-line:nth-child(2) div.single-harf:nth-child(3) #localcounter",
            attr: {
                "content": "Showing the position of the Harf inside the Surah",
                "placement": "top",
            }
        });
        items.push({
            selector: ".verse-line:nth-child(2) div.single-harf:nth-child(3) #globalcounter",
            attr: {
                "content": "Showing the position of the Harf inside the Quran",
                "placement": "bottom",
            }
        });
        items.push({
            selector: ".verse-line:first-child .surah-verse-no",
            attr: {
                "content": "Showing 'SurahNo':'VerseNo'",
                "placement": "bottom",
            }
        });
    }
    if (navFilter) {
        items.push({
            selector: "#StatLink",
            attr: {
                "content": "Clicking \"Statistics\" will bring the Statistics page.",
                "placement": "bottom",
            }
        });
        items.push({
            selector: "#Switch",
            preCallback: function () {
                setTimeout(function () {
                    if ($("#FilterForm").is(":hidden")) {
                        $("#Switch").click();
                    }
                }, 200);
            },
            attr: {
                "content": "Clicking \"Filter\" will open the filter section",
                "placement": "bottom",
            }
        });
        items.push({
            selector: "#FilterForm",
            attr: {
                "content": "This is the filter section.",
                "placement": "bottom",
            }
        });
        items.push({
            selector: "#SelectSurahFrom",
            attr: {
                "content": "select A Surah From ",
                "placement": "bottom",
            }
        });
        items.push({
            selector: "#SelectSurahTo",
            attr: {
                "content": "select A Surah To",
                "placement": "bottom",
            }
        });
        items.push({
            selector: "#SelectSingleHarf",
            attr: {
                "content": "Selecting a herf to see its Specific Positions",
                "placement": "bottom",
            }
        });
        items.push({
            selector: "#StartVerseNo",
            attr: {
                "content": " Select The Verse From",
                "placement": "bottom",
            }
        });
        items.push({
            selector: "#EndVerseNo",
            attr: {
                "content": "Select the Verse To",
                "placement": "bottom",
            }
        });
        items.push({
            selector: "#SelectSource",
            attr: {
                "content": "Get Different Sources",
                "placement": "bottom",
            }
        });
        //items.push({
        //    selector: "ul.nav.navbar-navbar-right",
        //    attr: {
        //        "content": "Showing 'SurahNo':'VerseNo'",
        //        "placement": "bottom",
        //    }
        //});
    }

    if (isHome) {
        var isLoggedIn = $("#LogOffLink").length > 0;
        if (isLoggedIn) {
            items.push({
                selector: ".verse-line:nth-child(2) a.edit-link",
                attr: {
                    "content": "Click edit icon if you want to edit this verse.",
                    "placement": "bottom",
                }
            });
        } else {
            items.push({
                selector: "#LogInLink",
                attr: {
                    "content": "If you are authorized user, you can use your google account to log in.",
                    "placement": "bottom",
                }
            });
        }
    }

    if (isEdit) {
        addEditPageItems(items);
    }

    if (isStat) {
        items.push({
            selector: "#HarfStatPieChart",
            attr: {
                "content": "The chart shows the count of all harfs in the selected range.",
                "placement": "bottom",
            }
        });
        items.push({
            selector: "#HarfPositionTable tr:first-child td:nth-child(1)",
            attr: {
                "content": "A single harf",
                "placement": "bottom",
            }
        });
        items.push({
            selector: "#HarfPositionTable tr:first-child td:nth-child(2)",
            attr: {
                "content": "All the local positions of the harf, based on current filter.",
                "placement": "bottom",
            }
        });
        items.push({
            selector: "#HarfPositionTable tr:first-child td:nth-child(3)",
            attr: {
                "content": "All the global positions of the haft, based on current filter.",
                "placement": "bottom",
            }
        });
    }

    //HarfPositionTable

    var onStep = function (args) { // params: {idx: activeIndex, direction: [next|prev]}
        if (args.direction === "next") {
            var pre = items[args.idx].preCallback;
            var post = items[args.idx].postCallback;
            if ($.isFunction(pre)) pre();
        }

        // alert("ID=" + args.idx + ", direction=" + args.direction);
    };

    for (var i = 0; i < items.length; i++) {
        var elem = $(items[i].selector);

        elem.addClass("bootstro");
        $.each(items[i].attr, function (name, value) {
            elem.attr("data-bootstro-" + name, value);
            elem.attr("data-bootstro-step", i);
        });
    }

    var options = {
        onStep: onStep,
        finishButton: ""
    };

    bootstro.start(".bootstro", options);
};

function addEditPageItems(items) {
    
}




/***** Trigger when the file loads ****************/

function tryRecalc() {
    var url = $("#RecalcUrl").val();
    $.getJSON(url);
}

$(document).ready(function () {
    window.setInterval(function () {
        tryRecalc();
    }, 10000);
});



