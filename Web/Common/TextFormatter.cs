﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class TextFormatter
    {
        public static string CleanHamza(string originalVerseText)
        {
            string cleanedText = originalVerseText.Replace("ؤ", "و");
            cleanedText = cleanedText.Replace("ئ", "ي");
            return cleanedText;
        }

        public static string GenerateInsertStatement(string verseText, int surahNo, int verseNo, int sourceId)
        {
            return $@"INSERT INTO [dbo].[Verse] ([SurahNo], [VerseNo], [VerseText], [SourceId], [OriginalText]) VALUES({surahNo}, {verseNo}, N'{verseText}', {sourceId}, N'{verseText}')";
        }

        public static string GenerateUpdateStatement(string verseText, int surahNo, int verseNo, int sourceId)
        {
            return $@"UPDATE [dbo].[Verse] SET [VerseText] = N'{verseText}', [OriginalText] = N'{verseText}', EditedBy='SYSTEM', EditedDate=GETDATE(), IsRecalculationRequired=1 WHERE SurahNo={surahNo} and VerseNo={verseNo} and SourceId={sourceId}";
        }


        public static string[] ParseWithHarkat(string originalVerseText)
        {
            List<string> items = new List<string>();
            bool inHarf = false;
            char[] allHarf = Constants.Harfs.Union(Constants.AdditionalHarfs).ToArray();
            string currentHarfWithHarkat = "";
            char[] escapeChar = new char[] { ' ' };

            for (int i = 0; i < originalVerseText.Length; i++)
            {
                char ch = originalVerseText[i];

                if (inHarf)
                {
                    if (allHarf.Contains(ch))
                    {
                        items.Add(currentHarfWithHarkat);
                        currentHarfWithHarkat = ch.ToString();
                    }
                    else if (escapeChar.Contains(ch))
                    {
                        if (currentHarfWithHarkat.Trim() != "")
                        {
                            items.Add(currentHarfWithHarkat);
                        }
                        items.Add(" ");
                        currentHarfWithHarkat = "";
                        inHarf = false;
                    }
                    else
                    {
                        currentHarfWithHarkat += ch.ToString();
                    }
                }
                else
                {
                    if (allHarf.Contains(ch))
                    {
                        inHarf = true;
                        currentHarfWithHarkat = ch.ToString();
                    }
                    else
                    {
                        currentHarfWithHarkat = "";
                    }
                }

                if (i == originalVerseText.Length - 1 && currentHarfWithHarkat.Trim() != "")
                {
                    items.Add(currentHarfWithHarkat);
                }
            }
            return items.ToArray();
        }

    }
}
