﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public enum SourceType
    {
        QuranComWithoutHarkat = 1,
        QuranComWithHarkat = 2,
        QuranWikiWithHarkat = 3
    }
}

