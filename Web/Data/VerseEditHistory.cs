﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class VerseEditHistory
    {
        public int Id { get; set; }
        public int SourceId { get; set; }
        public int VerseNo { get; set; }
        public int SurahNo { get; set; }
        public string EditedText { get; set; }
        public string EditedBy { get; set; }
        public DateTime EditedDate { get; set; }
    }
}

