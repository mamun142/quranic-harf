﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class Verse
    {
        public int Id { get; set; }
        public int SurahNo { get; set; }
        public int VerseNo { get; set; }
        public string VerseText { get; set; }
        public int StartSurahCounter { get; set; }
        public int StartGlobalCounter { get; set; }

        public int SourceId { get; set; }
        public string OriginalText { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedDate { get; set; }

        public int HarfCount { get; set; }
        public bool IsRecalculationRequired { get; set; }
    }
}
