﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class VerseHarf
    {
        public int Id { get; set; }
        public int VerseId { get; set; }
        public string HarfText { get; set; }
        public int LocalCounter { get; set; }
        public int GlobalCounter { get; set; }
        public int Serial { get; set; }
    }
}
