﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
   public class QuranicHarfDbContext: DbContext
    {
        public QuranicHarfDbContext()
        {
            Database.SetInitializer<QuranicHarfDbContext>(null);
        }

        public DbSet<Surah> Surahs { get; set; }

        public DbSet<Verse> Verses { get; set; }
        
        public DbSet<Source> Sources { get; set; }

        public DbSet<VerseHarf> VerseHarfs { get; set; }

        public DbSet<Config> Configs { get; set; }
        public DbSet<VerseEditHistory> VerseEditHistories { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

    }
}
