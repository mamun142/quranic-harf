﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
   public  class Surah
    { 
        public int Id { get; set; }
        public int SurahNo { get; set; }
        public string ArabicName { get; set; }
        public string EnglishName { get; set; }

    }
}
