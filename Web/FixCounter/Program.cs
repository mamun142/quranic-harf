﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using Common;

namespace FixCounter
{
    class Program
    {
        static void Main(string[] args)
        {
           // var counterFixer = new WithHarkatCounterFixer();
          var counterFixer = new HarkatLessCounterFixer();

            counterFixer.FixNow(1,2);

            Console.WriteLine("All verses are done.");
            Console.ReadKey();
        }

    }
}
