﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixCounter
{
    class HarkatLessCounterFixer : BaseCounterFixer
    {
        public override int GetHarfCount(string originalVerseText)
        {
            int harfCount = originalVerseText.Replace(" ", "").Length;
            return harfCount;
        }

        public override string[] GetHarfs(string originalVerseText)
        {
            return originalVerseText.ToCharArray().Select(c => c.ToString()).ToArray();
        }

        public override int GetSourceId()
        {
            int sourceId = (int)SourceType.QuranComWithoutHarkat;
            return sourceId;
        }
    }
}
