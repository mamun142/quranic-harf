﻿using Common;
using Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FixCounter
{
    abstract class BaseCounterFixer
    {
        private int GlobalCounter;
        public void FixNow(int startSurahNo = 1, int endSurahNo = 114)
        {
            int sourceId = GetSourceId();

            using (QuranicHarfDbContext context = new QuranicHarfDbContext())
            {
                if (startSurahNo == 1) GlobalCounter = 1;
                else
                {
                    int endVerseNo = Common.Constants.SurahVerseCounter[startSurahNo - 1];
                    Verse lastVerse = context.Verses.SingleOrDefault(v => v.SourceId == sourceId && v.SurahNo == startSurahNo - 1 && v.VerseNo == endVerseNo);
                    GlobalCounter = lastVerse.StartGlobalCounter + lastVerse.HarfCount;
                }
                var dateConfig = context.Configs.SingleOrDefault(c => c.Key == ConfigKey.LastFixCounterRunDate.ToString());
                dateConfig.Value = DateTime.UtcNow.ToString();

               
                var successConfig = context.Configs.SingleOrDefault(c => c.Key == ConfigKey.IsLastFixCounterRunSuccess.ToString());
                successConfig.Value = "false";
                context.SaveChanges();
            }

            for (int i = startSurahNo; i <= endSurahNo; i++)
            {
                FixBySurah(i, sourceId);
            }


            using (QuranicHarfDbContext context = new QuranicHarfDbContext())
            {
                var config = context.Configs.SingleOrDefault(c => c.Key == ConfigKey.IsLastFixCounterRunSuccess.ToString());
                config.Value = "true";
                context.SaveChanges();
            }

            /*using (QuranicHarfDbContext context = new QuranicHarfDbContext())
            {
               var varses = context.Verses.Where(v => v.EditedDate !=null && v.IsRecalculationRequired).ToList();
                var dateConfig = context.Configs.SingleOrDefault(c => c.Key == ConfigKey.LastFixCounterRunDate.ToString());
                dateConfig.Value = DateTime.UtcNow.ToString();
                foreach (var v in varses)
                {
                    string[] d = new string[] { v.EditedDate.ToString(), };
                    for(int i=0; i< d.Count(); i++)
                    { if (dateConfig.Value >=)
                     }
                }
            }*/
        }
        private void FixBySurah(int surahNo, int sourceId)
        {
            using (QuranicHarfDbContext context = new QuranicHarfDbContext())
            {
                List<Verse> allVerses = context.Verses.Where(v => v.SourceId == sourceId && v.SurahNo == surahNo).OrderBy(v => v.SurahNo).ThenBy(v => v.VerseNo).ToList();

                int surahCounter = 1;
                int verseCount = allVerses.Count;
                for (int i = 0; i < verseCount; i++)
                {
                    Verse verse = allVerses[i];

                    verse.StartSurahCounter = surahCounter;
                    verse.StartGlobalCounter = GlobalCounter;

                    int harfCount = GetHarfCount(verse.VerseText);
                    string[] harfs = GetHarfs(verse.VerseText);
                    verse.HarfCount = harfCount;

                    context.VerseHarfs.RemoveRange(context.VerseHarfs.Where(h => h.VerseId == verse.Id));
                    int verseHarfSerial = 1;
                    int verseHarfCounter = 0;
                    foreach (string harf in harfs)
                    {
                        var item = new VerseHarf()
                        {
                            HarfText = harf,
                            Serial = verseHarfSerial++,
                            VerseId = verse.Id,
                            LocalCounter = harf == " " ? 0 : surahCounter + verseHarfCounter,
                            GlobalCounter = harf == " " ? 0 : GlobalCounter + verseHarfCounter
                        };
                        if (harf != " ") verseHarfCounter++;
                        context.VerseHarfs.Add(item);
                        context.SaveChanges();
                    }

                    context.SaveChanges();

                    Console.WriteLine("Surah " + verse.SurahNo + " Verse " + verse.VerseNo + " saved in DB.");
                    surahCounter += harfCount;
                    GlobalCounter += harfCount;
                }
            }
        }

        public abstract int GetSourceId();

        public abstract int GetHarfCount(string originalVerseText);

        public abstract string[] GetHarfs(string originalVerseText);
    }
}
