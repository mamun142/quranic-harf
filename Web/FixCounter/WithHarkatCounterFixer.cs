﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixCounter
{
    class WithHarkatCounterFixer : BaseCounterFixer
    {
        private string[] harfsCache;
        private string cachedVerseText;
        public override int GetHarfCount(string originalVerseText)
        {
            return GetHarfs(originalVerseText).Where(h => h != " ").Count();
        }

        public override string[] GetHarfs(string originalVerseText)
        {
            string[] harfs;
            if (originalVerseText == cachedVerseText)
            {
                harfs = harfsCache;
            }
            else
            {
                harfs = harfsCache = TextFormatter.ParseWithHarkat(originalVerseText);
                cachedVerseText = originalVerseText;
            }
            return harfs;
        }

        public override int GetSourceId()
        {
            //int sourceId = (int)SourceType.QuranWikiWithHarkat;
            int sourceId = (int)SourceType.QuranComWithHarkat;
            return sourceId;
        }
    }
}
