﻿using Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuranWikiParser
{
    class Program
    {
        static void Main(string[] args)
        {
            int surahNo = 5;
            OutputType outputType = OutputType.SQLInsertQuery;// what type of String ?? Insert, Update or none

            int verseCount = Constants.SurahVerseCounter[surahNo];
            var urlGenerator = new QuranWikiUrlGenerator();
            string filePath = $@"C:\Users\Fatih1985\Desktop\quran-wiki\quran-wiki-{outputType}-{surahNo}.txt";

            using (var parser = new VerseParser())
            {
                File.WriteAllLines(filePath, new string[] { "-- Starting Surah: " + surahNo });
                for (int i = 1; i <= verseCount; i++)
                {
                    string url = urlGenerator.GenerateUrl(surahNo, i);
                    string verseText = parser.GetVerseText(url).Trim();

                    //Try again 
                    if(string.IsNullOrWhiteSpace(verseText))
                    {
                        verseText = parser.GetVerseText(url, 1000).Trim();
                    }

                    //If even fail second time 
                    if (string.IsNullOrWhiteSpace(verseText))
                    {
                        string errorText = $"ERROR!!! verseText is missing for surah={surahNo}, verse={i}, source={SourceType.QuranWikiWithHarkat}";
                        Console.WriteLine(errorText);
                        File.AppendAllLines(filePath, new string[] { errorText, Environment.NewLine });
                        File.AppendAllLines(filePath, new string[] { errorText, Environment.NewLine });
                        return;
                    }

                    // Quran wiki places bismillah-hir-rah-manir-rahim with ayah, which is not correct 
                    if (surahNo != 1 && i == 1) verseText = verseText.Replace("بِسْمِ ٱللَّهِ ٱلرَّحْمَٰنِ ٱلرَّحِيمِ", "");

                    string generatedText = "";

                    if (outputType == OutputType.ReadableText)
                    {
                        generatedText = verseText;
                    }
                    else if (outputType == OutputType.SQLInsertQuery)
                    {
                        generatedText = TextFormatter.GenerateInsertStatement(verseText, surahNo, i, (int)SourceType.QuranWikiWithHarkat);
                    }
                    else if (outputType == OutputType.SQLUpdateQuery)
                    {
                        generatedText = TextFormatter.GenerateUpdateStatement(verseText, surahNo, i, (int)SourceType.QuranWikiWithHarkat);
                    }

                    File.AppendAllLines(filePath, new string[] { "-- " + surahNo + ":" + i + " - ", generatedText, Environment.NewLine });
                }
            }
        }
    }
}
