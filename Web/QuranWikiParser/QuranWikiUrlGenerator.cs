﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuranWikiParser
{
    class QuranWikiUrlGenerator
    {
        private string urlTemplate = @"http://www.quran-wiki.com/ayat.php?sura={SurahNo}&aya={VerseNo}";

        public string GenerateUrl(int surahNo, int verseNo)
        {
            string url = urlTemplate.Replace("{SurahNo}", surahNo.ToString())
                                    .Replace("{VerseNo}", verseNo.ToString());
            return url;
        }

    }
}
