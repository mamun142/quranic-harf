﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QuranWikiParser
{
    class VerseParser : IDisposable
    {
        private IWebDriver _driver;
        public VerseParser()
        {
            _driver = new ChromeDriver();
            _driver.Manage().Window.Maximize();
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }

        public string GetVerseText(string url, int additionalWait = 0)
        {
            _driver.Navigate().GoToUrl(url);
            if (additionalWait > 0) Thread.Sleep(additionalWait);
            IWebElement span = WaitFindElement(By.CssSelector(".panel-body > span"));
            string verseText = span.Text.Trim();
            return verseText;
        }

        private IWebElement WaitFindElement(By by)
        {
            var wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(10));
            var myElement = wait.Until(x => x.FindElement(by));
            return myElement;
        }

        public void Dispose()
        {
            _driver.Quit();
        }


    }
}
