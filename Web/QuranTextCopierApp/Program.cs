﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using Newtonsoft.Json;
using System.IO;

namespace QuranTextCopierApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string filePath = @"C:\Users\Fatih1985\Desktop\qurac_text_copy.txt";

            //QuranComBaseExtractor extractor = new QuranComWithHarkatExtractor();
             QuranComBaseExtractor extractor = new QuranComWithoutHarkatExtractor();

            extractor.Extract(filePath, 1, 114, TargetType.SqlSelectQuery);

            Console.WriteLine("Done!");
        }
    }
}
