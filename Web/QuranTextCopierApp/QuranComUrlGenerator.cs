﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuranTextCopierApp
{
    class QuranComUrlGenerator
    {
        private string urlTemplate = @"https://quran.com/api/api/v3/chapters/{SurahNo}/verses?offset={Skip}&limit={Take}";

        public string GenerateUrl(int surahNo, int skipVerseCount, int takeVerses)
        {
            string url = urlTemplate.Replace("{SurahNo}", surahNo.ToString())
                                    .Replace("{Skip}", skipVerseCount.ToString())
                                    .Replace("{Take}", takeVerses.ToString());
            return url;
        }

    }
}
