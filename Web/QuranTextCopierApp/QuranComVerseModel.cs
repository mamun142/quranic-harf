﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuranTextCopierApp
{
    public class QuranComVerseModel
    {
        public int verse_number { get; set; }
        public int chapter_id { get; set; }
        public string text_indopak { get; set; }
        public string text_madani { get; set; }
        public string text_simple { get; set; }
        public string verse_key { get; set; }
    }
}
