﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuranTextCopierApp
{
    public class QuranComWithoutHarkatExtractor : QuranComBaseExtractor
    {
        public override int GetSourceId()
        {
            return (int)SourceType.QuranComWithoutHarkat;
        }

        public override string GetText(QuranComVerseModel verseModel)
        {
            return TextFormatter.CleanHamza(verseModel.text_simple);
        }
    }
}
