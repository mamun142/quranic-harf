﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuranTextCopierApp
{
    public class QuranComWithHarkatExtractor : QuranComBaseExtractor
    {
        public override int GetSourceId()
        {
            return (int)SourceType.QuranComWithHarkat;
        }

        public override string GetText(QuranComVerseModel verseModel)
        {
            return verseModel.text_madani;
        }
    }
}
