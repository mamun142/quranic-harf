﻿using Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuranTextCopierApp
{
    public abstract class QuranComBaseExtractor
    {
        public abstract string GetText(QuranComVerseModel verseModel);

        public abstract int GetSourceId();

        public void Extract(string filePath, int startSurah = 1, int endSurah = 144, TargetType target = TargetType.SqlSelectQuery)
        {
            int takePerCall = 10;

            QuranComUrlGenerator urlGenerator = new QuranComUrlGenerator();
            int totalAlreadyTaken = 0;
            for (int currentSurahIndex = startSurah; currentSurahIndex <= endSurah; currentSurahIndex++)
            {
                int takenInsideSurah = 0;
                int startVerse = 1;
                int endVerse = Constants.SurahVerseCounter[currentSurahIndex];

                File.AppendAllLines(filePath, new string[] { "-- Starting Surah: " + currentSurahIndex });
                for (int currentVerseIndex = startVerse; currentVerseIndex <= endVerse; currentVerseIndex += takePerCall)
                {
                    // Generate the url 
                    string url = urlGenerator.GenerateUrl(currentSurahIndex, takenInsideSurah, takePerCall);

                    // Call the api 
                    string json = JsonApiCaller.GetJsonData(url);

                    // Decode into object 
                    var apiModel = JsonConvert.DeserializeObject<QuranComApiModel>(json);

                    // Save in file as insert into statement 
                    foreach (QuranComVerseModel verseModel in apiModel.verses)
                    {
                        string quranText = GetText(verseModel);
                        if(target == TargetType.SqlSelectQuery)
                        {
                            string insertStatement = TextFormatter.GenerateInsertStatement(quranText, verseModel.chapter_id, verseModel.verse_number, GetSourceId());
                            File.AppendAllLines(filePath, new string[] { "-- " + verseModel.verse_key + " - ", insertStatement, Environment.NewLine });
                        }
                        else if (target == TargetType.PlainText)
                        {
                            File.AppendAllLines(filePath, new string[] { verseModel.verse_key + "-", quranText, Environment.NewLine });
                        }
                        Console.WriteLine("Verse taken: " + verseModel.verse_key + " " + quranText);
                    }

                    takenInsideSurah += takePerCall;
                    totalAlreadyTaken += takePerCall;
                }
            }
        }
    }
}
